<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B08698">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Maiesties letter Ianvary the 24th. In answer to the petition of both Houses of Parliament, as it was presented by the Earle of Newport, and the Lord Seymer. Ian. 21. 1641.</title>
    <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B08698 of text R175716 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C2391B). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 2 1-bit group-IV TIFF page images.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B08698</idno>
    <idno type="STC">Wing C2391B</idno>
    <idno type="STC">ESTC R175716</idno>
    <idno type="EEBO-CITATION">64551069</idno>
    <idno type="OCLC">ocm 64551069</idno>
    <idno type="VID">184448</idno>
    <idno type="PROQUESTGOID">2248530804</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B08698)</note>
    <note>Transcribed from: (Early English Books Online ; image set 184448)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2864:9)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Maiesties letter Ianvary the 24th. In answer to the petition of both Houses of Parliament, as it was presented by the Earle of Newport, and the Lord Seymer. Ian. 21. 1641.</title>
      <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
      <author>Newport, Mountjoy Blount, Earl of, ca. 1597-1666.</author>
      <author>Seymour, Francis, Baron Seymour of Trowbridge, 1590?-1664.</author>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([2] p.).</extent>
     <publicationStmt>
      <publisher>Printed for F. Coules and T. Bankes,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1641.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Imprint from colophon.</note>
      <note>Royal arms on top of broadside.</note>
      <note>Reproduction of original in: British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Charles -- I, -- King of England, 1600-1649.</term>
     <term>Great Britain -- History -- Charles I, 1625-1649 -- Sources.</term>
     <term>Great Britain -- Politics and government -- 1625-1649 -- Sources.</term>
     <term>Broadsides -- England -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>His Maiesties letter Ianuary the 24th. in answer to the petition of both Houses of Parliament, as it was presented by the Earle of Newport, aud [sic]</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1641</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>2</ep:pageCount>
    <ep:wordCount>280</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2010-06</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2010-06</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2010-07</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2010-07</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2011-06</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B08698-e10010">
  <body xml:id="B08698-e10020">
   <div type="letter" xml:id="B08698-e10030">
    <pb facs="tcp:184448:1" rend="simple:additions" xml:id="B08698-001-a"/>
    <head xml:id="B08698-e10040">
     <figure xml:id="B08698-e10050">
      <head xml:id="B08698-e10060">
       <w lemma="c" pos="sy" xml:id="B08698-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="B08698-001-a-0020">R</w>
      </head>
      <q xml:id="B08698-e10070">
       <w lemma="n/a" pos="ffr" xml:id="B08698-001-a-0030">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="B08698-001-a-0040">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="B08698-001-a-0050">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="B08698-001-a-0060">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="B08698-001-a-0070">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="B08698-001-a-0080">PENSE</w>
      </q>
      <figDesc xml:id="B08698-e10080">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="B08698-e10090">
     <w lemma="his" pos="po" xml:id="B08698-001-a-0090">HIS</w>
     <w lemma="majesty" pos="ng1" reg="MAJESTY'S" xml:id="B08698-001-a-0100">MAIESTIES</w>
     <w lemma="letter" pos="n1" xml:id="B08698-001-a-0110">LETTER</w>
     <w lemma="JANVARY" pos="nn1" reg="JANVARY" xml:id="B08698-001-a-0120">IANVARY</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-0130">the</w>
     <w lemma="24" pos="crd" xml:id="B08698-001-a-0140">24</w>
     <w lemma="the" pos="d" reg="th'" rend="hi" xml:id="B08698-001-a-0150">th</w>
     <pc unit="sentence" xml:id="B08698-001-a-0160">.</pc>
     <w lemma="in" pos="acp" xml:id="B08698-001-a-0170">In</w>
     <w lemma="answer" pos="n1" xml:id="B08698-001-a-0180">Answer</w>
     <w lemma="to" pos="acp" xml:id="B08698-001-a-0190">to</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-0200">the</w>
     <w lemma="petition" pos="n1" xml:id="B08698-001-a-0210">Petition</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-0220">of</w>
     <w lemma="both" pos="d" xml:id="B08698-001-a-0230">both</w>
     <w lemma="house" pos="n2" xml:id="B08698-001-a-0240">Houses</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-0250">of</w>
     <w lemma="parliament" pos="n1" xml:id="B08698-001-a-0260">Parliament</w>
     <pc xml:id="B08698-001-a-0270">,</pc>
     <w lemma="as" pos="acp" xml:id="B08698-001-a-0280">as</w>
     <w lemma="it" pos="pn" xml:id="B08698-001-a-0290">it</w>
     <w lemma="be" pos="vvd" xml:id="B08698-001-a-0300">was</w>
     <w lemma="present" pos="vvn" xml:id="B08698-001-a-0310">presented</w>
     <w lemma="by" pos="acp" xml:id="B08698-001-a-0320">by</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-0330">the</w>
     <w lemma="earl" pos="n1" reg="Earl" xml:id="B08698-001-a-0340">Earle</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-0350">of</w>
     <w lemma="Newport" pos="nn1" rend="hi" xml:id="B08698-001-a-0360">Newport</w>
     <pc xml:id="B08698-001-a-0370">,</pc>
     <w lemma="and" pos="cc" xml:id="B08698-001-a-0380">and</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-0390">the</w>
     <w lemma="lord" pos="n1" xml:id="B08698-001-a-0400">Lord</w>
     <w lemma="Seymer" pos="nn1" rend="hi" xml:id="B08698-001-a-0410">Seymer</w>
     <pc unit="sentence" xml:id="B08698-001-a-0420">.</pc>
     <w lemma="jan." pos="ab" reg="jan." xml:id="B08698-001-a-0430">Ian.</w>
     <w lemma="21." pos="crd" xml:id="B08698-001-a-0440">21.</w>
     <w lemma="1641." pos="crd" xml:id="B08698-001-a-0450">1641.</w>
     <pc unit="sentence" xml:id="B08698-001-a-0460"/>
    </head>
    <p xml:id="B08698-e10130">
     <w lemma="his" pos="po" rend="decorinit" xml:id="B08698-001-a-0470">HIs</w>
     <w lemma="majesty" pos="n1" xml:id="B08698-001-a-0480">Majesty</w>
     <w lemma="have" pos="vvg" xml:id="B08698-001-a-0490">having</w>
     <w lemma="see" pos="vvn" reg="seen" xml:id="B08698-001-a-0500">seene</w>
     <w lemma="and" pos="cc" xml:id="B08698-001-a-0510">and</w>
     <w lemma="consider" pos="vvn" xml:id="B08698-001-a-0520">considered</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-0530">the</w>
     <w lemma="petition" pos="n1" xml:id="B08698-001-a-0540">Petition</w>
     <w lemma="present" pos="vvn" xml:id="B08698-001-a-0550">presented</w>
     <w lemma="unto" pos="acp" xml:id="B08698-001-a-0560">unto</w>
     <w lemma="he" pos="pno" rend="hi" xml:id="B08698-001-a-0570">Him</w>
     <pc xml:id="B08698-001-a-0580">,</pc>
     <w lemma="the" pos="d" xml:id="B08698-001-a-0590">the</w>
     <w lemma="one" pos="crd" xml:id="B08698-001-a-0600">one</w>
     <w lemma="and" pos="cc" xml:id="B08698-001-a-0610">and</w>
     <w lemma="twenty" pos="ord" xml:id="B08698-001-a-0620">twentieth</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-0630">of</w>
     <w lemma="this" pos="d" xml:id="B08698-001-a-0640">this</w>
     <w lemma="instant" pos="n1-j" xml:id="B08698-001-a-0650">instant</w>
     <pc xml:id="B08698-001-a-0660">,</pc>
     <w lemma="by" pos="acp" xml:id="B08698-001-a-0670">by</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-0680">the</w>
     <w lemma="earl" pos="n1" xml:id="B08698-001-a-0690">Earl</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-0700">of</w>
     <w lemma="Newport" pos="nn1" xml:id="B08698-001-a-0710">Newport</w>
     <pc xml:id="B08698-001-a-0720">,</pc>
     <w lemma="and" pos="cc" xml:id="B08698-001-a-0730">and</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-0740">the</w>
     <w lemma="lord" pos="n1" xml:id="B08698-001-a-0750">Lord</w>
     <w lemma="Seymer" pos="nn1" xml:id="B08698-001-a-0760">Seymer</w>
     <pc xml:id="B08698-001-a-0770">,</pc>
     <w lemma="in" pos="acp" xml:id="B08698-001-a-0780">in</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-0790">the</w>
     <w lemma="name" pos="n2" xml:id="B08698-001-a-0800">names</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-0810">of</w>
     <w lemma="both" pos="d" xml:id="B08698-001-a-0820">both</w>
     <w lemma="house" pos="n2" rend="hi" xml:id="B08698-001-a-0830">Houses</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-0840">of</w>
     <w lemma="parliament" pos="n1" xml:id="B08698-001-a-0850">Parliament</w>
     <pc unit="sentence" xml:id="B08698-001-a-0860">.</pc>
     <w lemma="be" pos="vvz" xml:id="B08698-001-a-0870">Is</w>
     <w lemma="please" pos="vvn" xml:id="B08698-001-a-0880">pleased</w>
     <w lemma="to" pos="prt" xml:id="B08698-001-a-0890">to</w>
     <w lemma="return" pos="vvi" xml:id="B08698-001-a-0900">return</w>
     <w lemma="this" pos="d" xml:id="B08698-001-a-0910">this</w>
     <w lemma="answer" pos="n1" xml:id="B08698-001-a-0920">Answer</w>
     <pc unit="sentence" xml:id="B08698-001-a-0930">.</pc>
     <w lemma="that" pos="cs" xml:id="B08698-001-a-0940">That</w>
     <w lemma="he" pos="pns" reg="He" rend="hi" xml:id="B08698-001-a-0950">Hee</w>
     <w lemma="do" pos="vvz" xml:id="B08698-001-a-0960">doth</w>
     <w lemma="well" pos="av" xml:id="B08698-001-a-0970">well</w>
     <w lemma="approve" pos="vvi" xml:id="B08698-001-a-0980">approve</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-0990">of</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-1000">the</w>
     <w lemma="desire" pos="n1" xml:id="B08698-001-a-1010">desire</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-1020">of</w>
     <w lemma="both" pos="d" xml:id="B08698-001-a-1030">both</w>
     <w lemma="house" pos="n2" rend="hi" xml:id="B08698-001-a-1040">Houses</w>
     <pc xml:id="B08698-001-a-1050">,</pc>
     <w lemma="for" pos="acp" xml:id="B08698-001-a-1060">for</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-1070">the</w>
     <w lemma="speedy" pos="j" xml:id="B08698-001-a-1080">speedy</w>
     <w lemma="proceed" pos="n1-vg" xml:id="B08698-001-a-1090">proceeding</w>
     <w lemma="against" pos="acp" xml:id="B08698-001-a-1100">against</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-1110">the</w>
     <w lemma="person" pos="n2" xml:id="B08698-001-a-1120">persons</w>
     <w lemma="mention" pos="vvn" xml:id="B08698-001-a-1130">mentioned</w>
     <w lemma="in" pos="acp" xml:id="B08698-001-a-1140">in</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-1150">the</w>
     <w lemma="petition" pos="n1" xml:id="B08698-001-a-1160">Petition</w>
     <pc xml:id="B08698-001-a-1170">;</pc>
     <w lemma="whereof" pos="crq" xml:id="B08698-001-a-1180">whereof</w>
     <hi xml:id="B08698-e10180">
      <w lemma="his" pos="po" xml:id="B08698-001-a-1190">His</w>
      <w lemma="majesty" pos="n1" xml:id="B08698-001-a-1200">Majesty</w>
     </hi>
     <w lemma="find" pos="vvg" xml:id="B08698-001-a-1210">finding</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-1220">the</w>
     <w lemma="great" pos="j" xml:id="B08698-001-a-1230">great</w>
     <w lemma="inconvenience" pos="n2" xml:id="B08698-001-a-1240">inconveniences</w>
     <w lemma="by" pos="acp" xml:id="B08698-001-a-1250">by</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-1260">the</w>
     <w lemma="first" pos="ord" xml:id="B08698-001-a-1270">first</w>
     <w lemma="mistake" pos="n1" xml:id="B08698-001-a-1280">mistake</w>
     <w lemma="in" pos="acp" xml:id="B08698-001-a-1290">in</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-1300">the</w>
     <w lemma="way" pos="n1" xml:id="B08698-001-a-1310">way</w>
     <pc xml:id="B08698-001-a-1320">,</pc>
     <w lemma="have" pos="vvz" xml:id="B08698-001-a-1330">hath</w>
     <w lemma="endure" pos="vvn" xml:id="B08698-001-a-1340">endured</w>
     <w lemma="some" pos="d" xml:id="B08698-001-a-1350">some</w>
     <w lemma="delay" pos="n2" reg="delays" xml:id="B08698-001-a-1360">delayes</w>
     <pc xml:id="B08698-001-a-1370">;</pc>
     <w lemma="that" pos="cs" xml:id="B08698-001-a-1380">that</w>
     <w lemma="he" pos="pns" rend="hi" xml:id="B08698-001-a-1390">He</w>
     <w lemma="may" pos="vmd" xml:id="B08698-001-a-1400">might</w>
     <w lemma="be" pos="vvi" xml:id="B08698-001-a-1410">be</w>
     <w lemma="inform" pos="vvn" xml:id="B08698-001-a-1420">informed</w>
     <w lemma="in" pos="acp" xml:id="B08698-001-a-1430">in</w>
     <w lemma="what" pos="crq" xml:id="B08698-001-a-1440">what</w>
     <w lemma="order" pos="n1" xml:id="B08698-001-a-1450">Order</w>
     <w lemma="to" pos="prt" xml:id="B08698-001-a-1460">to</w>
     <w lemma="put" pos="vvi" xml:id="B08698-001-a-1470">put</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-1480">the</w>
     <w lemma="same" pos="d" xml:id="B08698-001-a-1490">same</w>
     <pc xml:id="B08698-001-a-1500">;</pc>
     <w lemma="but" pos="acp" xml:id="B08698-001-a-1510">But</w>
     <w lemma="before" pos="acp" xml:id="B08698-001-a-1520">before</w>
     <w lemma="that" pos="cs" xml:id="B08698-001-a-1530">that</w>
     <w lemma="that" pos="cs" xml:id="B08698-001-a-1540">that</w>
     <w lemma="be" pos="vvb" xml:id="B08698-001-a-1550">be</w>
     <w lemma="agree" pos="vvn" xml:id="B08698-001-a-1560">agreed</w>
     <w lemma="upon" pos="acp" xml:id="B08698-001-a-1570">upon</w>
     <pc xml:id="B08698-001-a-1580">,</pc>
     <hi xml:id="B08698-e10200">
      <w lemma="his" pos="po" xml:id="B08698-001-a-1590">His</w>
      <w lemma="majesty" pos="n1" xml:id="B08698-001-a-1600">Majesty</w>
     </hi>
     <w lemma="think" pos="vvz" reg="thinks" xml:id="B08698-001-a-1610">thinkes</w>
     <w lemma="it" pos="pn" xml:id="B08698-001-a-1620">it</w>
     <w lemma="unusual" pos="j" reg="unusual" xml:id="B08698-001-a-1630">unusuall</w>
     <pc xml:id="B08698-001-a-1640">,</pc>
     <w lemma="or" pos="cc" xml:id="B08698-001-a-1650">or</w>
     <w lemma="unfit" pos="j" xml:id="B08698-001-a-1660">unfit</w>
     <w lemma="to" pos="prt" xml:id="B08698-001-a-1670">to</w>
     <w lemma="discover" pos="vvi" xml:id="B08698-001-a-1680">discover</w>
     <w lemma="what" pos="crq" xml:id="B08698-001-a-1690">what</w>
     <w lemma="proof" pos="n1" reg="proof" xml:id="B08698-001-a-1700">proofe</w>
     <w lemma="be" pos="vvz" xml:id="B08698-001-a-1710">is</w>
     <w lemma="against" pos="acp" xml:id="B08698-001-a-1720">against</w>
     <w lemma="they" pos="pno" xml:id="B08698-001-a-1730">them</w>
     <pc xml:id="B08698-001-a-1740">,</pc>
     <w lemma="and" pos="cc" xml:id="B08698-001-a-1750">and</w>
     <w lemma="therefore" pos="av" xml:id="B08698-001-a-1760">therefore</w>
     <w lemma="hold" pos="vvz" xml:id="B08698-001-a-1770">holds</w>
     <w lemma="it" pos="pn" xml:id="B08698-001-a-1780">it</w>
     <w lemma="necessary" pos="j" xml:id="B08698-001-a-1790">necessary</w>
     <pc xml:id="B08698-001-a-1800">,</pc>
     <w lemma="lest" pos="cs" xml:id="B08698-001-a-1810">lest</w>
     <w lemma="a" pos="d" xml:id="B08698-001-a-1820">a</w>
     <w lemma="new" pos="j" xml:id="B08698-001-a-1830">new</w>
     <w lemma="mistake" pos="n1" xml:id="B08698-001-a-1840">mistake</w>
     <w lemma="shall" pos="vmd" xml:id="B08698-001-a-1850">should</w>
     <w lemma="breed" pos="vvi" xml:id="B08698-001-a-1860">breed</w>
     <w lemma="more" pos="dc" xml:id="B08698-001-a-1870">more</w>
     <w lemma="delay" pos="n2" reg="delays" xml:id="B08698-001-a-1880">delayes</w>
     <pc xml:id="B08698-001-a-1890">;</pc>
     <pc join="right" xml:id="B08698-001-a-1900">(</pc>
     <w lemma="which" pos="crq" xml:id="B08698-001-a-1910">which</w>
     <hi xml:id="B08698-e10210">
      <w lemma="his" pos="po" xml:id="B08698-001-a-1920">His</w>
      <w lemma="majesty" pos="n1" xml:id="B08698-001-a-1930">Majesty</w>
     </hi>
     <w lemma="to" pos="acp" xml:id="B08698-001-a-1940">to</w>
     <w lemma="his" pos="po" rend="hi" xml:id="B08698-001-a-1950">His</w>
     <w lemma="power" pos="n1" xml:id="B08698-001-a-1960">power</w>
     <w lemma="will" pos="vmb" xml:id="B08698-001-a-1970">will</w>
     <w lemma="avoid" pos="vvi" xml:id="B08698-001-a-1980">avoid</w>
     <pc xml:id="B08698-001-a-1990">.</pc>
     <pc unit="sentence" xml:id="B08698-001-a-2000">)</pc>
     <w lemma="that" pos="cs" xml:id="B08698-001-a-2010">That</w>
     <w lemma="it" pos="pn" xml:id="B08698-001-a-2020">it</w>
     <w lemma="be" pos="vvb" xml:id="B08698-001-a-2030">be</w>
     <w lemma="resolve" pos="vvn" xml:id="B08698-001-a-2040">resolved</w>
     <w lemma="whether" pos="cs" xml:id="B08698-001-a-2050">whether</w>
     <hi xml:id="B08698-e10230">
      <w lemma="his" pos="po" xml:id="B08698-001-a-2060">His</w>
      <w lemma="majesty" pos="n1" xml:id="B08698-001-a-2070">Majesty</w>
     </hi>
     <w lemma="be" pos="vvi" reg="be" xml:id="B08698-001-a-2080">bee</w>
     <w lemma="bind" pos="vvn" xml:id="B08698-001-a-2090">bound</w>
     <w lemma="in" pos="acp" xml:id="B08698-001-a-2100">in</w>
     <w lemma="respect" pos="n1" xml:id="B08698-001-a-2110">respect</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-2120">of</w>
     <w lemma="privilege" pos="n2" reg="Privileges" xml:id="B08698-001-a-2130">Priviledges</w>
     <w lemma="to" pos="prt" xml:id="B08698-001-a-2140">to</w>
     <w lemma="proceed" pos="vvi" xml:id="B08698-001-a-2150">proceed</w>
     <w lemma="against" pos="acp" xml:id="B08698-001-a-2160">against</w>
     <w lemma="they" pos="pno" xml:id="B08698-001-a-2170">them</w>
     <w lemma="by" pos="acp" xml:id="B08698-001-a-2180">by</w>
     <w lemma="impeachment" pos="n1" xml:id="B08698-001-a-2190">impeachment</w>
     <w lemma="in" pos="acp" xml:id="B08698-001-a-2200">in</w>
     <w lemma="parliament" pos="n1" xml:id="B08698-001-a-2210">Parliament</w>
     <pc xml:id="B08698-001-a-2220">;</pc>
     <w lemma="or" pos="cc" xml:id="B08698-001-a-2230">Or</w>
     <w lemma="whether" pos="cs" xml:id="B08698-001-a-2240">whether</w>
     <w lemma="he" pos="pns" rend="hi" xml:id="B08698-001-a-2250">He</w>
     <w lemma="be" pos="vvb" xml:id="B08698-001-a-2260">be</w>
     <w lemma="at" pos="acp" xml:id="B08698-001-a-2270">at</w>
     <w lemma="liberty" pos="n1" xml:id="B08698-001-a-2280">liberty</w>
     <w lemma="to" pos="prt" xml:id="B08698-001-a-2290">to</w>
     <w lemma="prefer" pos="vvi" xml:id="B08698-001-a-2300">prefer</w>
     <w lemma="a" pos="d" xml:id="B08698-001-a-2310">an</w>
     <w lemma="indictment" pos="n1" reg="Indictment" xml:id="B08698-001-a-2320">Inditement</w>
     <w lemma="at" pos="acp" xml:id="B08698-001-a-2330">at</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-2340">the</w>
     <w lemma="common" pos="j" xml:id="B08698-001-a-2350">Common</w>
     <w lemma="law" pos="n1" xml:id="B08698-001-a-2360">Law</w>
     <pc xml:id="B08698-001-a-2370">,</pc>
     <w lemma="in" pos="acp" xml:id="B08698-001-a-2380">in</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-2390">the</w>
     <w lemma="usual" pos="j" reg="usual" xml:id="B08698-001-a-2400">usuall</w>
     <w lemma="way" pos="n1" xml:id="B08698-001-a-2410">way</w>
     <pc xml:id="B08698-001-a-2420">,</pc>
     <w lemma="or" pos="cc" xml:id="B08698-001-a-2430">or</w>
     <w lemma="have" pos="vvb" xml:id="B08698-001-a-2440">have</w>
     <w lemma="his" pos="po" rend="hi" xml:id="B08698-001-a-2450">His</w>
     <w lemma="choice" pos="n1" xml:id="B08698-001-a-2460">choice</w>
     <w lemma="of" pos="acp" xml:id="B08698-001-a-2470">of</w>
     <w lemma="either" pos="d" xml:id="B08698-001-a-2480">either</w>
     <pc xml:id="B08698-001-a-2490">:</pc>
     <w lemma="whereupon" pos="crq" xml:id="B08698-001-a-2500">Whereupon</w>
     <hi xml:id="B08698-e10260">
      <w lemma="his" pos="po" xml:id="B08698-001-a-2510">His</w>
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="B08698-001-a-2520">Majesties</w>
     </hi>
     <w lemma="desire" pos="n1" xml:id="B08698-001-a-2530">desire</w>
     <w lemma="to" pos="prt" xml:id="B08698-001-a-2540">to</w>
     <w lemma="satisfy" pos="vvi" reg="satisfy" xml:id="B08698-001-a-2550">satisfie</w>
     <w lemma="both" pos="d" xml:id="B08698-001-a-2560">both</w>
     <w lemma="house" pos="n2" rend="hi" xml:id="B08698-001-a-2570">Houses</w>
     <pc xml:id="B08698-001-a-2580">,</pc>
     <w lemma="and" pos="cc" xml:id="B08698-001-a-2590">and</w>
     <w lemma="to" pos="prt" xml:id="B08698-001-a-2600">to</w>
     <w lemma="put" pos="vvi" xml:id="B08698-001-a-2610">put</w>
     <w lemma="a" pos="d" xml:id="B08698-001-a-2620">a</w>
     <w lemma="determination" pos="n1" xml:id="B08698-001-a-2630">determination</w>
     <w lemma="to" pos="acp" xml:id="B08698-001-a-2640">to</w>
     <w lemma="the" pos="d" xml:id="B08698-001-a-2650">the</w>
     <w lemma="business" pos="n1" reg="business" xml:id="B08698-001-a-2660">businesse</w>
     <pc unit="sentence" xml:id="B08698-001-a-2670">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="B08698-e10280">
   <div type="colophon" xml:id="B08698-e10290">
    <p xml:id="B08698-e10300">
     <w lemma="London" pos="nn1" rend="hi" xml:id="B08698-001-a-2680">London</w>
     <pc xml:id="B08698-001-a-2690">,</pc>
     <w lemma="print" pos="vvn" xml:id="B08698-001-a-2700">Printed</w>
     <w lemma="for" pos="acp" xml:id="B08698-001-a-2710">for</w>
     <hi xml:id="B08698-e10320">
      <w lemma="f." pos="ab" xml:id="B08698-001-a-2720">F.</w>
      <w lemma="Coules" pos="nn1" xml:id="B08698-001-a-2730">Coules</w>
     </hi>
     <pc rend="follows-hi" xml:id="B08698-001-a-2740">,</pc>
     <w lemma="and" pos="cc" xml:id="B08698-001-a-2750">and</w>
     <hi xml:id="B08698-e10330">
      <w lemma="t." pos="ab" xml:id="B08698-001-a-2760">T.</w>
      <w lemma="bank" pos="n2" reg="Banks" xml:id="B08698-001-a-2770">Bankes</w>
     </hi>
     <pc rend="follows-hi" unit="sentence" xml:id="B08698-001-a-2780">.</pc>
     <w lemma="1641." pos="crd" xml:id="B08698-001-a-2790">1641.</w>
     <pc unit="sentence" xml:id="B08698-001-a-2800"/>
    </p>
    <pb facs="tcp:184448:2" rend="simple:additions" xml:id="B08698-002-a"/>
   </div>
  </back>
 </text>
</TEI>
